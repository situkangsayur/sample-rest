/**
 * Created by hendri_k on 11/16/15.
 */
var data = require('./../structures');

var express = require('express');

var router = express.Router();

//res.status(1).send({"code" : 1, "status" : "error saat pencarian"});

/**
 * select all mahasiswa
 * @param req
 * @param res
 * @param next
 */
allMahasiswa = function(req, res, next){

    data.mhs.find({}, function(err,docs){
        if(err){
            res.json({code : 1, message : "error : data is empty"});
        }else{
            res.json({code : 0, data : docs});
        }
    })
}


/**
 * add new mahasiswa
 * @param req
 * @param res
 */
addMahasiswa = function(req, res){
    var req_data = req.body;
    if((req_data.nim == null)|| (req_data.nama == null) || (req_data.alamat == null)){
        res.json({ code : 3, message : "data is not complete"});
    } else{
        new data.mhs({
            nim : req_data.nim,
            nama : req_data.nama,
            alamat : req_data.alamat,
            umur : req_data.umur,
            orang_tua : {
                ibu : req_data.orang_tua.ibu,
                bapa :req_data.orang_tua.bapa
            },
            social_network: {
                fb : req_data.social_network.fb,
                twitter : req_data.social_network.twitter
            },
            created_at : req_data.created_at
        }).save(function(err, doc){
                if(err){
                    res.json({code : 3, message : "adding new mahasiswa was failed"});
                }else{
                    res.json({code : 0, data : doc});
                }
            });
    }

}

/**
 * search by name
 * @param req
 * @param res
 * @param next
 */
searchByName =  function(req, res, next){
    var req_data =req.params;
    data.mhs.findOne({nama : req_data.nama}, function(err,docs){
        if(err){
            res.json(errors.getError(1));
        }else{
            if(docs == ""){
                res.json(code : 4, message :"search was failed"));
            }else{
                res.json({code: 0 , data : docs})
            }
        }
    })
}


/**
 * update data mahasiswa
 * @param req
 * @param res
 * @param next
 */
updateMahasiswa = function(req, res, next){
    var req_data = req.body;

    if((req_data.old_id == null)|| (req_data.id == null) || (req_data.nama == null) || (req_data.alamat == null)){
        res.json(errors.getError(4));
    } else{
        data.mhs.update(
            {
                id : req_data.old_id
            },
            {
                $set :
                {
                    nim : req_data.nim,
                    nama : req_data.nama,
                    alamat : req_data.alamat,
                    umur : req_data.umur,
                    orang_tua : {
                        ibu : req_data.orang_tua.ibu,
                        bapa :req_data.orang_tua.bapa
                    },
                    social_network: {
                        fb : req_data.social_network.fb,
                        twitter : req_data.social_network.twitter
                    },
                    created_at : req_data.created_at
                }
            }, function(err){
                if(err){
                    res.json({code : 7, message : "update was failed"});
                }else{
                    res.json({code : 0, message :"success"});
                }
            });
    }
}

/**
 * delete data mahasiswa
 * @param req
 * @param res
 * @param next
 */
deleteMahasiswa = function(req, res, next){

    data.mhs.remove(
        {
            id : req.body.id
        }, function(err){
            if(err){
                res.json({code : 5 , message : "delete was failed"});
            }else{
                res.json({code : 0, message : "success"});
            }
        });

}


router.get('/allmahasiswa', allMahasiswa);
router.get('/mahasiswa/:nama', searchByName);
router.post('/add/mahasiswa', addMahasiswa);
router.put('/edit/mahasiswa', updateMahasiswa);
router.delete('/delete/mahasiswa', deleteMahasiswa);

module.exports = router;
