var should = require('should');
var assert = require('assert');
var request = require('supertest');


/**
 * testing for mahasiswa end point
 */
describe('Routing API : ', function(){

    /**
     * add new mahasiswa
     */
    describe('Add new mahasiswa', function() {
        it('return documents of new user if the feature is right', function (done) {
            var dateTime = new Date().toISOString();

            var mhsProfile = JSON.stringify({
                    nim : "123456",
                    nama : "Tono",
                    alamat : "sekeloa",
                    umur : 22,
                    orang_tua : {
                        ibu : "Ibunya Tono",
                        bapa : "Bapanya Tono"
                    },
                    social_network: {
                        fb : "http://facebook.com/tono",
                        twitter : "https://twitter.com/tonoimoet"
                    },
                    created_at : dateTime
            });

            request('localhost:3000')
                .post('/api/add/mahasiswa')
                .set({'Content-Type': 'application/json'})
                .send(mhsProfile)
                .expect('Content-Type', /json/)
                .expect(200) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }

                    res.body.should.have.property('code');
                    res.body.data.nim.should.equal('123456');
                    res.body.data.nama.should.equal('Tono');
                    res.body.data.alamat.should.equal('sekeloa');
                    res.body.data.umur.should.equal(22);
                    res.body.data.orang_tua.ibu.should.equal('Ibunya Tono');
                    res.body.data.orang_tua.bapa.should.equal('Bapanya Tono');
                    res.body.data.social_network.fb.should.equal("http://facebook.com/tono");
                    res.body.data.social_network.twitter.should.equal('https://twitter.com/tonoimoet');
                    res.body.data.created_at.should.equal(dateTime);

                    done();

                });
        });
    });

    describe('select all mahasiswa', function() {


        it('return all documents of new user if the collection is not empty', function (done) {
            request('localhost:3000')
                .get('/api/allmahasiswa')
                .expect('Content-Type', /json/)
                .expect(200) //Status code
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }

                    res.body.data.length > 0;

                    done();

                });
        });
    });


});