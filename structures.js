var connection = require('./utils/utils');
var mongoose = connection.mongo;

mahasiswa = new mongoose.Schema({
    nim : {
        type : String,
        requred :true
    },
    nama : String,
    alamat : String,
    umur : Number,
    orang_tua : {
        ibu : String,
        bapa :String
    },
    social_network: {
        fb : String,
        twitter : String
    },
    created_at : Date
});

exports.mhs = mongoose.model('mahasiswa', mahasiswa);